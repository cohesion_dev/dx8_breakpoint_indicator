## DX8 Breakpoint indicator

When enabled, this module provides an indicator in the bottom left of your browser window to show which breakpoint you are viewing your page at.

### Steps to install

To install the module using composer follow these steps. 

Navigate to the document root in your terminal and run the following commands.

```
composer config repositories.dx8-break-point vcs git@bitbucket.org:cohesion_dev/dx8_breakpoint_indicator.git
composer require cohesion/dx8_breakpoint_indicator
```

You can enable the module using the drush command below or through the UI.

```
drush en dx8_breakpoint_indicator
```